#include "pracownik.h"
#include "pracowniknazlecenie.h"
#include "pracownikstaly.h"
#include "handlowiec.h"
#include <iostream>
#include <vector>

using namespace std;

int main()
{
    vector< Pracownik * > pracownicy;

    PracownikStaly *p1 = new PracownikStaly("Jakub", "Banaszkiewicz", "0", "0");
    PracownikNaZlecenie *p2 = new PracownikNaZlecenie("Piotr", "Banaszkiewicz", "0", "0");
    Handlowiec *p3 = new Handlowiec("Marek", "Banaszkiewicz", "0", "0");

    p1->pensja = 2000;
    p2->stawka_godzinowa = 14;
    p2->ilosc_nadgodzin = 10;
    p3->sprzedaz = 40000;
    p3->procent = 0.1;

    pracownicy.push_back(p1);
    pracownicy.push_back(p2);
    pracownicy.push_back(p3);

    vector< Pracownik * >::iterator it1;
    for (it1 = pracownicy.begin(); it1 != pracownicy.end(); it1++)
    {
        cout << "Pracownik zarabia " << (*it1)->wylicz_zarobki() << endl
             << "### ";
        (*it1)->print();
    }
    return 0;
}
