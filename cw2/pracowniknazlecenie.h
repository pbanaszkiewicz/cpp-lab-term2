#ifndef _PRACOWNIKNAZLECENIE_H_
#define _PRACOWNIKNAZLECENIE_H_ 1

#include "pracownik.h"

class PracownikNaZlecenie: public Pracownik
{
    public:
        int stawka_godzinowa;
        unsigned int ilosc_nadgodzin;

        PracownikNaZlecenie(std::string imie, std::string nazwisko,
                            std::string NIP, std::string PESEL)
            : Pracownik(imie, nazwisko, NIP, PESEL)
        {}
        virtual ~PracownikNaZlecenie()
        {
            std::cerr << "Klasa PracownikNaZlecenie zostala usunieta"
                      << std::endl;
        }

        virtual void print()
        {
            std::cout << nazwisko << " " << imie << " (" << NIP << ", "
                      << PESEL << "): " << stawka_godzinowa << "/h"
                      << std::endl;
        }
        virtual double wylicz_zarobki()
        {
            return stawka_godzinowa * 40 * 4 + ilosc_nadgodzin * 1.5 * stawka_godzinowa;
        }
};

#endif
