#ifndef _PRACOWNIKSTALY_H_
#define _PRACOWNIKSTALY_H_ 1

#include "pracownik.h"

class PracownikStaly: public Pracownik
{
    public:
        int pensja;

        PracownikStaly(std::string imie, std::string nazwisko, std::string NIP,
                       std::string PESEL)
            : Pracownik(imie, nazwisko, NIP, PESEL)
        {}
        virtual ~PracownikStaly()
        {
            std::cerr << "Klasa PracownikStaly zostala usunieta" << std::endl;
        }

        virtual void print()
        {
            std::cout << nazwisko << " " << imie << " (" << NIP << ", "
                      << PESEL << "): " << pensja << std::endl;
        }
        virtual double wylicz_zarobki()
        {
            return pensja;
        }
};

#endif
