#ifndef _HANDLOWIEC_H_
#define _HANDLOWIEC_H_ 1

#include "pracownik.h"

class Handlowiec: public Pracownik
{
    public:
        int sprzedaz;
        double procent;

        Handlowiec(std::string imie, std::string nazwisko, std::string NIP,
                   std::string PESEL)
            : Pracownik(imie, nazwisko, NIP, PESEL)
        {}
        virtual ~Handlowiec()
        {
            std::cerr << "Klasa Handlowiec zostala usunieta" << std::endl;
        }

        virtual void print()
        {
            std::cout << nazwisko << " " << imie << " (" << NIP << ", "
                      << PESEL << "): " << sprzedaz * procent << std::endl;
        }
        virtual double wylicz_zarobki()
        {
            return sprzedaz * procent;
        }
};

#endif
