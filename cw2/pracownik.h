#ifndef _PRACOWNIK_H_
#define _PRACOWNIK_H_ 1

#include <string>
#include <iostream>

class Pracownik
{
    public:
        std::string imie;
        std::string nazwisko;
        std::string NIP;
        std::string PESEL;

        Pracownik(std::string imie, std::string nazwisko, std::string NIP,
                  std::string PESEL)
            : imie(imie), nazwisko(nazwisko), NIP(NIP), PESEL(PESEL)
        {}
        virtual ~Pracownik()
        {
            std::cerr << "Klasa Pracownik zostala usunieta" << std::endl;
        }

        virtual void print()
        {
            std::cout << nazwisko << " " << imie << " (" << NIP << ", "
                      << PESEL << ")" << std::endl;
        }
        virtual double wylicz_zarobki() = 0;
};

#endif
