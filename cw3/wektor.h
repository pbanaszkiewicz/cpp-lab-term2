#ifndef _WEKTOR_H_
#define _WEKTOR_H_ 1

#include <algorithm>
#include <iostream>
#include <iterator>
#include <cstring>

template <class T>
class wektor
{
public:
    // T* _type_ptr;
    // typedef T* iterator;
    // typename T* iterator;
    typedef T* iterator;
    typedef const T* const_iterator;

    wektor();
    wektor(unsigned int count);
    wektor(const wektor &w);
    ~wektor();

    iterator begin()
    {
        return array;
    }

    iterator end()
    {
        return array + size_;
    }

    void push_back(T);
    void push_front(T);
    T pop_back();
    T pop_front();

    void insert(unsigned int, T);
    T erase(unsigned int);
    T at(unsigned int);

    void empty()
    {
        delete [] array;
        size_ = 0;
        allocate_back(inc_size_);
    }

    unsigned int length()
    {
        return size_;
    }

    T operator[](unsigned int index)
    {
        return at(index);
    }

private:
    T *array;
    unsigned long size_;
    unsigned long allocated_;
    unsigned long inc_size_;

    // allocate specific size (absolute, not relative!!!)
    void allocate_back(unsigned long);
    void allocate_front(unsigned long);
};

/*****************************************************************************/

template <class T>
wektor<T>::wektor()
    : array(0), size_(0), allocated_(0), inc_size_(5)
{
    allocate_back(inc_size_);
}

template <class T>
wektor<T>::wektor(unsigned int count)
    : array(0), size_(0), allocated_(0), inc_size_(5)
{
    allocate_back(count);
}

template <class T>
wektor<T>::wektor(const wektor &w)
{
    std::memcpy(array, w.array, w.allocated_ * sizeof(T));
}

template <class T>
wektor<T>::~wektor()
{
    delete [] array;
}

/*****************************************************************************/

// template <class T>
// wektor<T>::iterator wektor<T>::begin()
// {
//     return array;
// }

// template <class T>
// wektor<T>::iterator wektor<T>::end()
// {
//     return array + size_;
// }

/*****************************************************************************/

template <class T>
void wektor<T>::allocate_front(unsigned long size)
{
    T* array2 = new T[size];
    // copy blindly, no matter if dest is smaller than src
    std::memcpy(array2 + 1, array, size_ * sizeof(T));
    delete [] array;

    array = array2;
    // size_ = size;  // size doesn't really change -- REMEMBER: it's blind!
    allocated_ = size;  // but allocated size does change
}

template <class T>
void wektor<T>::allocate_back(unsigned long size)
{
    T* array2 = new T[size];
    // copy blindly, no matter if dest is smaller than src
    std::memcpy(array2, array, size_ * sizeof(T));
    delete [] array;

    array = array2;
    // size_ = size;  // size doesn't really change -- REMEMBER: it's blind!
    allocated_ = size;  // but allocated size does change
}

/*****************************************************************************/

template <class T>
void wektor<T>::push_front(T element)
{
    allocate_front(allocated_ + 1);
    array[0] = element;
    size_++;
}

template <class T>
void wektor<T>::push_back(T element)
{
    if (size_ < allocated_)
    {
        array[size_] = element;
        // std::cerr << "size_ < allocated_" << std::endl;
    }
    else
    {
        allocate_back(allocated_ + inc_size_);
        array[size_] = element;
    }

    size_++;
}

template <class T>
T wektor<T>::pop_back()
{
    T result = at(length() - 1);

    allocated_--;
    T *array2 = new T[allocated_];
    std::memcpy(array2, array, allocated_ * sizeof(T));
    delete [] array;
    array = array2;

    size_--;

    return result;
}

template <class T>
T wektor<T>::pop_front()
{
    T result = at(0);

    allocated_--;
    T *array2 = new T[allocated_];
    std::memcpy(array2, array + 1, (allocated_ + 1) * sizeof(T));
    delete [] array;
    array = array2;

    size_--;

    return result;
}

template <class T>
void wektor<T>::insert(unsigned int index, T element)
{
    if (index == 0)
        return push_front(element);

    else if (index == size_)
        return push_back(element);

    else
    {
        T* array2 = new T[allocated_ + 1];

        // copy first part of the array
        std::memcpy(array2, array, index * sizeof(T));

        // insert the element
        array2[index] = element;

        // copy second part of the array
        std::memcpy(array2 + index + 1, array + index, (size_ - 1) * sizeof(T));

        delete [] array;
        array = array2;

        size_++;
        allocated_++;
    }
}

template <class T>
T wektor<T>::erase(unsigned int index)
{
    if (index == 0)
        return pop_front();

    else if (index == size_)
        return pop_back();

    else
    {
        T* array2 = new T[allocated_ - 1];

        // copy first part of the array
        std::memcpy(array2, array, (index - 1) * sizeof(T));

        // obtain the element
        T result = array[index];

        // copy second part of the array
        std::memcpy(array2 + index, array + index + 1, (size_ - index) * sizeof(T));

        delete [] array;
        array = array2;

        size_--;
        allocated_--;

        return result;
    }
}

template <class T>
T wektor<T>::at(unsigned int index)
{
    return array[index];
}

#endif
