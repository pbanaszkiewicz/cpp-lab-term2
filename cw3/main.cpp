#include "wektor.h"
#include "sklep.h"
#include <iostream>
#include <string>
#include <sstream>
using namespace std;

int main()
{
    cout << "--------------------------------------------------------------\n";
    string input_addresses =
        "Toys     ul.Konarska 33\n"
        "Zabawki  ul.Chelmonskiego 13\n"
        "Pluszaki ul.Brygadierow  5\n"
        "Toys     ul.Szucha 9\n";

    string input_toys =
        "Toys     tygrys   9.50\n"
        "Toys     lalka   20.0\n"
        "Pluszaki  mis     4.0\n";

    stringstream sinput_addr(input_addresses);

    wektor<Sklep> shops;
    wektor<Sklep>::iterator shops_it;
    wektor<Budynek> buildings;
    wektor<Budynek>::iterator buildings_it;
    wektor<Przedmiot> toys;
    wektor<Przedmiot>::iterator toys_it;

    string nazwa_sklepu, ulica; int numer_bud;
    while (sinput_addr >> nazwa_sklepu >> ulica >> numer_bud)
    {
        Sklep ptr;
        bool found = false;

        // znajdz sklep wsrod juz istniejacych
        for (shops_it = shops.begin(); shops_it != shops.end(); shops_it++)
        {
            if (shops_it->name == nazwa_sklepu)
            {
                ptr = *shops_it;
                found = true;
                break;
            }
        }

        // dodaj nowy sklep jesli jeszcze nie istnieje
        if (!found)
        {
            Sklep new_sklep; new_sklep.name = nazwa_sklepu;
            shops.push_back(new_sklep);
            ptr = shops.at(shops.length() - 1);
        }

        // dodaj adres nowego sklepu
        Budynek new_budynek;
        new_budynek.shop = ptr;
        new_budynek.address = ulica;
        new_budynek.building_number = numer_bud;
        buildings.push_back(new_budynek);
    }

    stringstream sinput_toys(input_toys);

    string nazwa_zabawki; double cena;
    while (sinput_toys >> nazwa_sklepu >> nazwa_zabawki >> cena)
    {
        Sklep ptr;

        // znajdz sklep wsrod juz istniejacych (na pewno tam jest)
        for (shops_it = shops.begin(); shops_it != shops.end(); shops_it++)
        {
            if (shops_it->name == nazwa_sklepu)
            {
                ptr = *shops_it;
                break;
            }
        }

        Przedmiot toy;
        toy.name = nazwa_zabawki;
        toy.price = cena;
        toy.shop = ptr;

        toys.push_back(toy);
    }

    // wyswietlenie wszystkich zabawek w poszczegolnych sklepach wraz z cenami
    for (shops_it = shops.begin(); shops_it != shops.end(); shops_it++)
    {
        for (toys_it = toys.begin(); toys_it != toys.end(); toys_it++)
        {
            if (toys_it->shop.name != shops_it->name) continue;

            for (buildings_it = buildings.begin();
                 buildings_it != buildings.end(); buildings_it++)
            {
                if (buildings_it->shop.name != shops_it->name) continue;

                cout << shops_it->name << "\t" << toys_it->name << "\t"
                     << toys_it->price << "\t" << buildings_it->address << "\t"
                     << buildings_it->building_number << endl;
            }
        }
    }

    return 0;
}
