#ifndef _SKLEP_H_
#define _SKLEP_H_ 1

#include <string>

struct Sklep
{
    std::string name;
};

struct Budynek
{
    Sklep shop;
    std::string address;
    int building_number;
};

struct Przedmiot
{
    Sklep shop;
    std::string name;
    double price;
};

#endif
