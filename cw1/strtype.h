#include <fstream>
#include <istream>

typedef enum {
    ALPHA,
    ALPHA_NUM,
    NON_WHITE,
    NOT_NEW,
} InType;

const unsigned int MAX_CHARS = 100;

class StrType
{
public:
    void MakeEmpty();
    void GetString(bool skip, InType charsAllowed);
    void GetStringFile(bool skip, InType charsAllowed, std::ifstream &inFile);
    void PrintToScreen(bool newLine);
    void PrintToFile(bool newLine, std::ofstream &outFile);
    int LengthIs();
    void CopyString(StrType &newString);
private:
    char letters[MAX_CHARS+1];
    void appendChar(char);
    void _GetString(bool skip, InType charsAllowed, std::istream &input);
    void _Print(bool newline, std::ostream &output);
};

