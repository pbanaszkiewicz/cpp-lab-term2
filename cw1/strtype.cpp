#include <cstring>
#include <iostream>
#include "strtype.h"

void StrType::MakeEmpty()
{
    char letters_new[] = "";
    std::memcpy(this->letters, letters_new, sizeof this->letters);
}

void StrType::appendChar(char c)
{
    char letters_new[] = {c, '\0'};
    strcat(this->letters, letters_new);
}

void StrType::_GetString(bool skip, InType charsAllowed, std::istream &input)
{
    if (!skip)
    {
        input.getline(this->letters, MAX_CHARS+1);
        //std::getline(input, this->letters);
    }

    while (!input.eof())
    {
        char c = input.get();

        switch (charsAllowed)
        {
            case ALPHA:
                if (isalpha(c)) appendChar(c);
                break;

            case ALPHA_NUM:
                if (isalnum(c)) appendChar(c);
                break;

            case NON_WHITE:
                if (!isblank(c)) appendChar(c);
                break;

            case NOT_NEW:
                if (c != '\10') appendChar(c);
                break;
        }
    }

}

void StrType::GetString(bool skip, InType charsAllowed)
{
    _GetString(skip, charsAllowed, std::cin);
}

void StrType::GetStringFile(bool skip, InType charsAllowed, std::ifstream &inFile)
{
    _GetString(skip, charsAllowed, inFile);
}

void StrType::_Print(bool newline, std::ostream &output)
{
    output << letters;
    if (newline) output << std::endl;
}

void StrType::PrintToScreen(bool newline)
{
    this->_Print(newline, std::cout);
}

void StrType::PrintToFile(bool newline, std::ofstream &outFile)
{
    this->_Print(newline, outFile);
}

int StrType::LengthIs()
{
    return std::strlen(this->letters);
}

void StrType::CopyString(StrType &newString)
{

}

