#ifndef _LISTNODE_H_
#define _LISTNODE_H_ 1

template <class T>
class List;

template <class T>
class ListNode
{
private:
    ListNode(const T &data, ListNode *prevPtr, ListNode *nextPtr)
        : item(data), prev(prevPtr), next(nextPtr)
    { }

    T item;
    ListNode *prev, *next;

    friend class List<T>;
};

#endif
