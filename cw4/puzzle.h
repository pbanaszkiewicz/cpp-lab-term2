#ifndef _PUZZLE_H_
#define _PUZZLE_H_ 1

#include "list.h"

class Puzzle
{
public:
    Puzzle(char answer[], char initial[]);
    // ~Puzzle();

    void shiftLeft();
    void swapEnds();
    void display();
    bool isSolved();

    unsigned int moveIndexLeft();
    unsigned int moveIndexRight();

private:
    List<char> solution, puzzle;
    unsigned int index;
};

#endif
