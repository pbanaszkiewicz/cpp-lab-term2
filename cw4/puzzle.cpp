#include "puzzle.h"
#include <cstring>
#include <cassert>

Puzzle::Puzzle(char answer[], char initial[])
    : index(0)
{
    unsigned int length = std::strlen(answer);
    assert (length == std::strlen(initial));

    for (unsigned int i = 0; i < length; ++i)
    {
        solution.insert(answer[i]);
        puzzle.insert(initial[i]);
    }
    solution.gotoBeginning();
    puzzle.gotoBeginning();
}


void Puzzle::shiftLeft()
// shift current letter one position to the left
{
    puzzle.gotoBeginning();

    for (unsigned int i = 0; i < index; i++)
    {
        puzzle.gotoNext();
    }

    char current = puzzle.getCursor(), previous;

    puzzle.gotoPrev();
    previous = puzzle.getCursor();

    puzzle.replace(current);
    puzzle.gotoNext();
    puzzle.replace(previous);
    puzzle.gotoPrev();

    puzzle.gotoBeginning();

    moveIndexLeft();
}

void Puzzle::swapEnds()
{
    char beginning, end;

    puzzle.gotoBeginning();
    beginning = puzzle.getCursor();

    puzzle.gotoEnd();
    end = puzzle.getCursor();

    puzzle.replace(beginning);
    puzzle.gotoBeginning();
    puzzle.replace(end);
}

void Puzzle::display()
{
    std::cout << solution.length() << std::endl;
    std::cout << puzzle.length() << std::endl;
    for (unsigned int i = 0; i < puzzle.length(); i++)
    {
        std::cout << puzzle.getCursor();
        puzzle.gotoNext();
    }
    std::cout << std::endl;

    for (unsigned int i = 0; i < index; i++)
        std::cout << " ";
    std::cout << "^" << std::endl;
}

bool Puzzle::isSolved()
{
    puzzle.gotoBeginning();
    solution.gotoBeginning();
    for (int i = 0; i < puzzle.length(); ++i)
    {
        if (puzzle.getCursor() != solution.getCursor())
            return false;

        puzzle.gotoNext();
        solution.gotoNext();
    }
    puzzle.gotoBeginning();
    solution.gotoBeginning();
    return true;
}

unsigned int Puzzle::moveIndexLeft()
{
    // forgot that modulo division in C++ DOESN'T WORK LIKE IT'S SUPPOSED TO
    index = (index == 0 ? puzzle.length() - 1 : index - 1);
    return index;
}

unsigned int Puzzle::moveIndexRight()
{
    index = (index + 1) % solution.length();
    return index;
}
