#ifndef _LIST_H_
#define _LIST_H_ 1

#include <iostream>
#include <new>
#include <stdexcept>
#include "listnode.h"

template <class T>
class List
{
public:
    List()
        : cursor(NULL), first(NULL), size_(0), reversed(false)
    { }
    ~List()
    {
        clear();
    }

    /* methods working on specific item in the list */
    void insert(const T &item)
        throw (std::bad_alloc);
    void remove()
        throw (std::logic_error);
    void replace(const T &item)
        throw (std::logic_error);

    /* methods operating on the size of the list */
    void clear();
    bool isEmpty() const
    {
        return size_ == 0;
    }
    bool isFull() const
    {
        return size_ != 0;
    }
    unsigned int length() const
    {
        return size_;
    }

    /* methods manipulating cursor position */
    void gotoBeginning()
        throw (std::logic_error);
    void gotoEnd()
        throw (std::logic_error);
    void gotoPrev()
        throw (std::logic_error);
    void gotoNext()
        throw (std::logic_error);

    /* obtain ListNode's data that cursor is pointing at */
    T getCursor() const
        throw (std::logic_error);

    void showStructure() const;

    /* access elements in reversed mode */
    /* BETA */
    void reverse()
    {
        reversed = !reversed;
    }

private:
    ListNode<T> *cursor;
    ListNode<T> *first;
    unsigned int size_;
    bool reversed;

};

/*****************************************************************************/

template <class T>
void List<T>::insert(const T &item)
    throw (std::bad_alloc)
// insert new ListNode item RIGHT AFTER cursor position
{
    // std::bad_alloc exception is thrown by new operator

    if (!first)
    {
        // the list is empty
        first = new ListNode<T>(item, NULL, NULL);
        ListNode<T> *tmp = first;
        first->next = tmp;
        first->prev = tmp;

        cursor = first;
    }
    else
    {
        ListNode<T> *tmp = cursor->next;
        cursor->next = new ListNode<T>(item, cursor, tmp);
        tmp->prev = cursor->next;

        tmp = cursor->next;
        cursor = tmp;
    }

    size_++;
}

template <class T>
void List<T>::remove()
    throw (std::logic_error)
// remove item under cursor
{
    ListNode<T> *prev = cursor->prev, *next = cursor->next;

    delete cursor;

    cursor = next;
    next->prev = prev;
    prev->next = next;

    size_--;
}

template <class T>
void List<T>::replace(const T &item)
    throw (std::logic_error)
// swap cursors' data value with another
{
    cursor->item = item;
}

/*****************************************************************************/

template <class T>
void List<T>::clear()
    // Remove exactly N-elements from the list.  The assumption is that
    // the list has exactly N-elements, so it's very important to always
    // remember to update size_!
{
    ListNode<T> *tmp = first, *tmp2;
    for (unsigned int i = 0; i < size_; ++i)
    {
        tmp2 = tmp->next;
        delete tmp;
        tmp = tmp2;
    }
    size_ = 0;
}

/*****************************************************************************/

template <class T>
void List<T>::gotoBeginning()
    throw (std::logic_error)
{
    cursor = first;
}

template <class T>
void List<T>::gotoEnd()
    throw (std::logic_error)
{
    cursor = first->prev;
}

template <class T>
void List<T>::gotoPrev()
    throw (std::logic_error)
{
    ListNode<T> *ptr = cursor->prev;
    cursor = ptr;
}

template <class T>
void List<T>::gotoNext()
    throw (std::logic_error)
{
    ListNode<T> *ptr = cursor->next;
    cursor = ptr;
}

/*****************************************************************************/

template <class T>
T List<T>::getCursor() const
    throw (std::logic_error)
// return cursor's item
{
    return cursor->item;
}

/*****************************************************************************/

template <class T>
void List<T>::showStructure() const
{
    ListNode<T> *ptr = cursor;

    do
    {
        std::cout << ptr->item << " <-> ";
        ptr = ptr->next;
    } while (ptr != cursor);

    std::cout << "loop..." << std::endl;
}

#endif
