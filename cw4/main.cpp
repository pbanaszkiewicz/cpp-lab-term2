#include <iostream>
#include <cstdlib>
#include <ctime>
#include "puzzle.h"
using namespace std;

int main()
{
    srand(time(0));

    char words[][2][50] =
    {
        {"zadanie", "adzeani"},
        {"informatyka", "karmanotify"},
        {"laptop", "paplot"},
        {"zegarek", "arkgeez"}
    };

    unsigned int pswd = rand() % 4;

    Puzzle puzzle(words[pswd][0], words[pswd][1]);

    cout << "l - przesun index o 1 pozycje w lewo\n"
            "r - przesun index o 1 pozycje w prawo\n"
            "s - zamien pierwsza i ostatnia litere zagadki miejscami\n"
            "L - przesun litere pod indeksem w lewo\n"
            "C - sprawdz rozwiazanie\n"
            "q lub Q - wyjscie\n"
            "-------------------------------------------------------";

    cout << "Zagadka na dzis:" << endl;
    puzzle.display();

    bool playing = true, win = false;
    char option;
    do
    {
        cout << ">>> ";
        cin >> option;

        switch (option)
        {
            case 'l':
                puzzle.moveIndexLeft();
                break;

            case 'r':
                puzzle.moveIndexRight();
                break;

            case 's':
                puzzle.swapEnds();
                break;

            case 'L':
                puzzle.shiftLeft();
                break;

            case 'C':
                win = puzzle.isSolved();
                playing = !win;
                if (win)
                    cout << "Gratulacje!" << endl;
                else
                    cout << "Niestety, probuj dalej" << endl;
                break;

            case 'Q':
            case 'q':
                playing = false;
                break;

            default:
                cout << "Klawisz nierozpoznany!" << endl;
        }

        if (playing)
            puzzle.display();
    } while (playing);

    // win or lose?

    return 0;
}
